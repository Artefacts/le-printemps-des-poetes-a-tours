# Printemps des Poètes à Tours

Site www.leprintempsdespoetesatours.com de l'association Le Printemps de Poètes à Tours.

![Website screenshot](wp-content/themes/prin-poetes-02/screenshot.png "Website screenshot")

## Thème "prin-poetes-02"

Thème pour le site www.leprintempsdespoetesatours.com.

Merci à:
- Bootstrap v4
- jQuery 1.x
- Wordpress 5.x
- Php 7.x
- and so many other free software contributors <3

## Technique

### Envoi des emails

Les emails ne sont pas bien délivrés via l'hébergement actuel, ajout de l'action `phpmailer_init` pour configurer le mailer de Wordpress. La configuration est définie dans `wp-config.php` avec la clé `'MAIL_CONFIG'`.

### CSS

- [How to Give a Div Element 100% Height of the Browser Window](https://www.w3docs.com/snippets/css/how-to-give-a-div-tag-100-height-of-the-browser-window.html)
