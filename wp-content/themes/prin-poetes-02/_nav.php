<div class="container-fluid">
<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
  <a class="mr-2" href="/">
    <img src="/wp-content/themes/prin-poetes-02/img/logo PdP Tours.png" width="40" height="40" alt="Logo Printemps des poètes à Tours">
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarCollapse">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="/" title="Accueil">Accueil</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/#agenda" title="Agenda">Agenda</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/#ecouter-regarder" title="Écouter-regarder">Écouter-regarder</a>
      </li>
      <li class="nav-item">
      <a class="nav-link" href="/#ressources" title="Ressources">Ressources</a>
      </li>
      <li class="nav-item">
      <a class="nav-link" href="/#qui-sommes-nous" title="Qui sommes nous ?">Qui sommes nous ?</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/#archives" title="Archives">Archives</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/#contact" title="Contacts">Contacts</a>
      </li>
    </ul>
    <!--
    <form class="form-inline mt-2 mt-md-0">
      <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
    </form>
-->
  </div>
</nav>
</div>