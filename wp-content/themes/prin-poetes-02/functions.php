<?php
/**
 * 
 * @package pdpat
 */

require_once(__DIR__.'/src/Common.php');

use PrinPoetes\Common ;
use PrinPoetes\Admin ;
use PrinPoetes\Front ;

/**
 * Support for Featured Image
 * https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
 */
add_theme_support( 'post-thumbnails' ); 

add_action( 'after_setup_theme', [Common::class, 'wp_after_setup_theme'] );
add_action( 'wp_enqueue_scripts', [Common::class, 'wp_enqueue_scripts'], 10 );

if( is_blog_admin() )
{
    require_once(__DIR__.'/src/Admin.php');

    Admin::events_admin();
}
else if( is_admin() )
{
}
else
{
    require_once(__DIR__.'/src/Front.php');

    add_action( 'wp_enqueue_scripts', [Front::class, 'wp_enqueue_scripts'], 20 );
    add_action( 'pre_get_posts', [Front::class, 'pre_get_posts_events']);

    add_filter('body_class', [Front::class, 'wp_body_class'], 10, 1);
    add_filter('excerpt_more', [Front::class, 'wp_excerpt_more']);
    //add_filter('post_thumbnail_html', [PrinPoetes::class, 'wp_post_thumbnail_html'], 10, 5 );    

}

function custom_phpmailer( $phpmailer )
{
    $conf = MAIL_CONFIG ;
    $phpmailer->isSMTP();     
    $phpmailer->Host = $conf['host'];
    // Ask it to use authenticate using the Username and Password properties
    $phpmailer->SMTPAuth = $conf['auth'];
    $phpmailer->Port = $conf['port'];
    if( $conf['auth'] )
    {
        $phpmailer->Username = $conf['username'];
        $phpmailer->Password = $conf['password'];    
    }
    // Choose 'ssl' for SMTPS on port 465, or 'tls' for SMTP+STARTTLS on port 25 or 587
    $phpmailer->SMTPSecure = $conf['secure'];
    // Additional settings…
    //$phpmailer->From = "you@yourdomail.com";
    //$phpmailer->FromName = "Your Name";
}
if( defined('MAIL_CONFIG') )
    add_action( 'phpmailer_init', 'custom_phpmailer' );
