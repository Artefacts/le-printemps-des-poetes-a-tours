<?php
/**
 * The archive-events template.
 * 
 * Events order is done in Front::pre_get_posts()
 * 
 * @package pdpat
 */

use PrinPoetes\Common;

get_header();

include(__DIR__ . '/_nav.php');

// Show SQL :
//global $wp_query ;
//Common::debug( 'archive-events', $wp_query->request);

?>

<section class="container-fluid" >
    <div class="row" style="">
        <div class="col-12">
            <article>
                <div class="container">
                    <div class="row">
                        <div class="col">
                        <?php
                            $q = new \WP_Query(['pagename'=>Common::SLUG_AGENDA_INTRO]);
                            $p = $q->get_posts();
                            echo $p[0]->post_content ;
                        ?>
                        </div>
                    </div>
                </div>
            </article>
        </div>
    </div>
    <div class="row mb-4">
        <div class="col" style="">

            <div class="container" >
                <div class="row" style="">
            <?php if ( have_posts() ) { ?>

                <?php while ( have_posts() ) { ?>

                        <div class="col-12 col-sm-6 col-xl-4 d-flex flex-grow-1" >
                            <?php 
                            the_post();
                            include(__DIR__.'/_event.php')
                            ?>
                        </div>

                <?php } ?>

            <?php } ?>
            </div><!--row-->
            </div><!--container-->

        </div><!--col-->
    </div><!--row-->
    <div class="row mt-2 mb-4">
        <div class="col-12">
            <div class="text-center">

                <a href="" class="btn read-more go-back"
                    onclick="window.history.back(); return false;">Retour</a>

            </div>
        </div>
    </div>

</section><!--container-fluid-->

<?php

get_footer();
