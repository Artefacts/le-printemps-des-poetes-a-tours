<?php

/**
 * The page template.
 * 
 * @package pdpat
 */

get_header();

include(__DIR__ . '/_nav.php');

use PrinPoetes\Common;

$datetime = date_create(get_post_meta($post->ID, Common::ACF_DATESTART, true));
if( $datetime->format('H') == 0 )
    $date_start = $datetime->format('d/m/Y');
else
    $date_start = $datetime->format('d/m/Y à H\hi');

$date_end = get_field(Common::ACF_DATEEND, $post->ID);
if ($date_end) {
    $datetime = date_create($date_end);
    if( $datetime->format('H') == 0 )
        $date_end = $datetime->format('d/m/Y');
    else
        $date_end = $datetime->format('d/m/Y à H\hi');
}

?>
<style>
    article {
        /*min-height: 90vh ;*/
        /*display: flex;
        align-items: center;*/
    }

    article table td {
        font-size: 1.5rem;
    }

    article table.dates td span,
    article table.lieu td span {
        vertical-align: baseline;
    }

    article ul.terms {
        list-style-type: none;
        padding-inline-start: 0;
    }

    article ul.terms li {
        display: inline-block;
        background-color: #ffda00;
        border: 1px solid #dcbc00;
        padding-left: .5rem;
        padding-right: .5rem;
        border-radius: .5rem;
    }
</style>

<div class="container-fluid" style="min-height: 100vh;">
    <div class="row" >
        <div class="col-12">

            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                <div class="container">

                    <!-- thumb -->
                    <?php
                    $thumb_html = get_the_post_thumbnail($post->ID);
                    if ($thumb_html) { ?>
                        <div class="row mb-1">
                            <div class="col text-center">
                                <?php echo $thumb_html; ?>
                            </div>
                        </div>
                    <?php
                    }
                    ?>

                    <!-- title -->
                    <div class="row">
                        <div class="col">
                            <div class="entry-content">
                                <?php the_title('<h1>', '</h1>'); ?>
                            </div>
                        </div>
                    </div>

                    <!-- dates -->
                    <div class="row">
                        <div class="col">
                            <table class="m-auto dates">
                                <tr>
                                    <td style="padding: 4px"><span class="dashicons dashicons-calendar-alt"></span></td>
                                    <td>
                                        <?php if ($date_end) { ?>
                                            <span style="">du <span class="badge badge-light"><?php echo $date_start ?></span></span>
                                            <br />
                                            <span style="">au <span class="badge badge-light"><?php echo $date_end ?></span></span>
                                        <?php } else { ?>
                                            <span style="">le <span class="badge badge-light"><?php echo $date_start ?></span></span>
                                        <?php } ?>

                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>

                    <div class="row mt-1">
                        <!-- location -->
                        <div class="col">
                            <table class="m-auto lieu">
                                <tr>
                                    <td style="padding: 4px"><span class="dashicons dashicons-location"></span></td>
                                    <td>
                                        <?php echo get_post_meta($post->ID, Common::ACF_LIEU, true) ?>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>

                    <div class="row mt-1">
                        <div class="col">
                            <!-- terms -->
                            <ul class="terms text-center">
                                <?php
                                /**
                                 * @var \WP_Term $term
                                 */
                                foreach (wp_get_post_terms($post->ID, Common::TAX_EVENTS) ?? [] as $term) {
                                ?>
                                    <li><?php echo $term->name ?></li>
                                <?php
                                }
                                ?>
                            </ul>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col">
                            <div class="entry-content">

                                <?php the_content(); ?>

                            </div>
                        </div>
                    </div>


                </div>
                <!--container-->

            </article>
        </div>
    </div>

    <div class="row mt-2 mb-4">
        <div class="col-12">
            <div class="text-center">

                <a href="" class="btn read-more go-back"
                    onclick="window.history.back(); return false;">Retour</a>

            </div>
        </div>
    </div>

</div><!--container-fluid-->

<?php

get_footer();
