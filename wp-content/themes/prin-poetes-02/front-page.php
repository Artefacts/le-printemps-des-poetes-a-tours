<?php
/**
 * The front-page template.
 * 
 * @package pdpat
 */

get_header();

use PrinPoetes\Common ;

?>
<div id="first" class="container-fluid">

<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
  <a class="mr-2" href="/">
    <img src="/wp-content/themes/prin-poetes-02/img/logo PdP Tours.png" width="40" height="40" alt="Logo Printemps des poètes à Tours">
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarCollapse">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link page-scroll" href="#agenda" title="Agenda">Agenda</a>
      </li>
      <li class="nav-item">
        <a class="nav-link page-scroll" href="#ecouter-regarder" title="Écouter regarder">Écouter-regarder</a>
      </li>
      <li class="nav-item">
      <a class="nav-link page-scroll" href="#ressources" title="Ressources">Ressources</a>
      </li>
      <li class="nav-item">
      <a class="nav-link page-scroll" href="#qui-sommes-nous" title="Qui sommes nous ?">Qui sommes nous ?</a>
      </li>
      <li class="nav-item">
        <a class="nav-link page-scroll" href="#archives" title="Archives">Archives</a>
      </li>
      <li class="nav-item">
        <a class="nav-link page-scroll" href="#contact" title="Contacts">Contacts</a>
      </li>
    </ul>
    <!--
    <form class="form-inline mt-2 mt-md-0">
      <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
    </form>
-->
  </div>
</nav>

<style>

h2 {
    font-weight: bolder;
    font-size: 2rem;
}
h3 {
    font-weight: bold;
    font-size: 1.5rem;
}

section {
    min-height: 100vh!important;
}

section .title h2 {
    color: white;
}
section .title img {
    width: 200px;
}

#first {
    background: radial-gradient(circle at 80% 100% , #F08D21 32%,#506CB4 60%, #2C3D67 90%);
}
@media(min-width:768px) {
    #first {
        background: radial-gradient(circle at 100% 100% , #FFBE41 20%, #FA7038 30%, #B5A59A 60%);
    }
}


#ecouter-regarder {
    background: radial-gradient(circle 40rem at 0 10% , #506CB4 40%, #EB8B22 70%,#2C3D67 80%);
}
@media(min-width:768px) {
    #ecouter-regarder {
        background: radial-gradient(circle 80rem at 0 10% , #506CB4 40%, #EB8B22 70%,#2C3D67 80%);
    }
}

#archives {
    background: radial-gradient(circle at 7% 100% , #506CB4 5%, #F08D21 18%,#7F7F7F 50%);
}
#contact {
    background: radial-gradient(circle at 80% 100% , #F08D21 16%,#506CB4 40%, #2C3D67 70%);
}

/*
    #acceuil ===
*/

#accueil {
    padding-top: 8rem;
    padding-bottom: 4rem;;
}
#accueil > .row {
    min-height: 70vh!important;
}

#accueil h1 {
    font-size: 2rem;
    margin-top: 2rem ;
    margin-bottom: 2rem ;
}

#coup-de-coeur img {
    width: 90%; height: auto;
    margin-top: 8vw;
}

#accueil .col-citation {
    background-color: rgba(228,228,228,.4);
    font-size: .9rem;
    margin-top: 1rem;
    padding-top: 2rem;
    padding-left: 0;
    padding-right: 0;
}
#accueil .citation {
    color: #182983 ;
}
#accueil .citation p {
    margin-bottom: 0.5rem;
    line-height: 1.1;
}

#accueil .citation p.author {
    line-height: 1.2;
    text-indent: 0;
    margin-left: 10rem;
}

@media (min-width: 576px)
{
    #accueil h1 {
    font-size: 2rem;
    margin-top: 10vw ;
}

}
@media (min-width: 768px)
{
    #accueil h1 {
        font-size: 2rem;
        margin-top: 5vw ;
    }
    #accueil .col-citation {
        margin-top: 0;
        margin-left: 12px;
        padding-top: 1rem;
        font-size: 1rem;
    }
    #accueil .citation p {
        margin-bottom: 0.4rem;
        line-height: 1.2;
    }
    #accueil .citation {
        text-indent: -58px;
    }
    #accueil .citation span {
        color: #7EA8ED ;
    }

}
@media (min-width: 992px)
{
    #accueil h1 {
        font-size: 2rem;
        margin-top: 8vw ;
    }

}
@media (min-width: 1200px)
{
    #accueil h1 {
        font-size: 2.5rem;
        margin-top: 7vw ;
    }
}

/*
    #agenda ===
*/


/*
    #ressources ===
*/

#ressources .card {
    color: #e9ecef;
    background-color: #ce4e1f;
}

/*
    #qui-sommes-nous ===
*/

#qui-sommes-nous .card {
    color: #e9ecef;
    background-color: #ce4e1f;
}

/*
    #contact ===
*/

#contact .card {
    color: #e9ecef;
    background-color: #F58A3B;
}
#contact .card a {
    color: black;
    font-style: italic;
    text-decoration: underline;
}

/*
    #archives ===
*/

#archives .card {
    color: #e9ecef;
    background-color: #F58A3B;
}
#archives .card a {
    color: black;
    font-style: italic;
    text-decoration: underline;
}

/*
    #ecouter-regarder ===
*/

#ecouter-regarder .card {
    color: #e9ecef;
    background-color: #F58A3B;
}
#ecouter-regarder .card a {
    color: black;
    font-style: italic;
    text-decoration: underline;
}

</style>

<section id="accueil" class="container-fluid ">
    <div class="row ">
        <div class="col-12 col-md-8 col-lg-8 d-flex flex-column" style="background-color: #4A66AC;">
            <div class="container-fluid flex-grow-1 d-flex flex-column" >
                <div class="row ">
                    <div class="col-12 col-sm-6 col-md-7 col-lg-5" >
                        <img src="/wp-content/themes/prin-poetes-02/img/logo PdP Tours - big.png"
                            class="img-fluid" style="margin-left: -1rem; margin-top: -2rem" alt="Logo Le Printemps des Poètes à Tours" />
                    </div>
                    <div class="col-12 col-sm-6 col-md-12 col-lg-7 text-center" >
                        <h1 >Le Printemps<br/>des Poètes - Tours</h1>
                    </div>
                </div>
                <div class="row align-content-end align-items-end" id="coup-de-coeur">
                    <div class="col-lg-4">
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-8">
                            <?php include_once(__DIR__.'/front-page-parts/_sticky-post.php') ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col col-citation" >
            <div class="container-fluid" >
                <div class="row">
                    <div class="col">
                        <div class="citation">
                            <p><span>Des</span> poèmes, ce sont aussi des présents.</p>
                            <p><span>Des</span> présents destinés aux attentifs.</p>
                            <p><span>Des</span> présents porteurs de destin.</p>
                            <p class="author">Paul Celan,
                                <br/><em style="font-size:80%">Le méridien, 1960</em>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row d-flex row-pdp-nat" >
                    <div class="col align-self-end">
                        <!--
                        <img src="/wp-content/themes/prin-poetes-02/img/logo PdP national.jpg" class="img-fluid d-block mx-auto " />
-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

</div><!-- #first -->

<section id="agenda" class="container-fluid">
    <div class="row pt-5 pb-5" style="min-height: 100vh">
        <div class="col-12 col-sm-4 col-md-3">
            <div class="col text-center title">
                <img class="img-fluid" src="/wp-content/themes/prin-poetes-02/img/GL Poemes 2019.jpg" alt="Grand Livre Poèmes 2019"/>
                <h2 class="mt-2">L’agenda</h2>
            </div>
        </div>
        <div class="col-12 col-sm-7 col-md-8 d-flex align-items-center">

            <!-- template must set variable $readmore_link. -->
            <?php include_once(__DIR__.'/front-page-parts/_agenda.php') ?>

        </div>
        <div class="col-12 text-center">
        <?php if( $readmore_link ) { ?>
                <div >
                    <a href="<?php echo $readmore_link ?>" class="btn read-more" >Voir tout l’agenda</a>
                </div>
            <?php } ?>
        </div>
    </div>
</section>

<section id="ecouter-regarder" class="container-fluid">
    <div class="row pt-5 pb-5">
        <div class="col-12 col-sm-4 col-md-3">
            <div class="container">
                <div class="row">
                    <div class="col text-center title">
                        <img class="img-fluid" src="/wp-content/themes/prin-poetes-02/img/GL Poemes 2018.jpg" alt="Grand Livre Poèmes 2018"/>
                        <h2 class="mt-2">Écouter-regarder</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-sm-7 col-md-8 col-xl-8 d-flex align-items-center">

            <!-- template must set variable $readmore_link. -->
            <?php include_once(__DIR__.'/front-page-parts/_ecouter-regarder.php') ?>

        </div>
        <div class="col-12 text-center">
            <?php if( $readmore_link ) { ?>
                <div >
                    <a href="<?php echo $readmore_link ?>" class="btn read-more" >Voir toutes les ressources</a>
                </div>
            <?php } ?>
        </div>
    </div>
</section>

<section id="ressources" class="container-fluid">
    <div class="row pt-5 pb-5">
        <div class="col-12 col-sm-4 col-md-3">
            <div class="col text-center title">
                <img class="img-fluid" src="/wp-content/themes/prin-poetes-02/img/GL Poemes 2021.jpg" alt="Grand Livre Poèmes 2021"/>
                <h2 class="mt-2">Ressources</h2>
            </div>
        </div>
        <div class="col-12 col-sm-7 col-md-8 col-xl-8 d-flex align-items-center">

            <!-- template must set variable $readmore_link. -->
            <?php include_once(__DIR__.'/front-page-parts/_ressources.php') ?>

        </div>
        <div class="col-12 text-center">
            <?php if( $readmore_link ) { ?>
                <div >
                    <a href="<?php echo $readmore_link ?>" class="btn read-more" >Voir toutes les ressources</a>
                </div>
            <?php } ?>
        </div>
    </div>
</section>


<section id="qui-sommes-nous" class="container-fluid">
    <div class="row pt-5 pb-5" style="min-height: 100vh">
        <div class="col-12 col-sm-4 col-md-3">
            <div class="col text-center title">
                <img class="img-fluid" src="/wp-content/themes/prin-poetes-02/img/GL Poemes 2020.jpg" alt="Grand Livre Poèmes 2020"/>
                <h2 class="mt-2">Qui sommes nous ?</h2>
            </div>
        </div>
        <div class="col-12 col-sm-7 col-md-8 col-xl-8 d-flex align-items-center">

            <!-- template must set variable $readmore_link. -->
            <?php include_once(__DIR__.'/front-page-parts/_qui.php') ?>

        </div>
        <div class="col-12 text-center">
            <?php if( $readmore_link ) { ?>
                <div >
                    <a href="<?php echo $readmore_link ?>" class="btn read-more" >Voir toute l’histoire</a>
                </div>
            <?php } ?>
        </div>
    </div>
</section>

<section id="archives" class="container-fluid">
    <div class="row pt-5 pb-5">
        <div class="col-12 col-sm-4 col-md-3">
            <div class="container">
                <div class="row">
                    <div class="col text-center title">
                        <img class="img-fluid" src="/wp-content/themes/prin-poetes-02/img/GL Poemes 2017.jpg" alt="Grand Livre Poèmes 2017"/>
                        <h2 class="mt-2">Archives</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-sm-7 col-md-8 col-xl-8 d-flex align-items-center">

            <!-- template must set variable $readmore_link. -->
            <?php include_once(__DIR__.'/front-page-parts/_archives.php') ?>

        </div>
    </div>
</section>

<section id="contact" class="container-fluid">
    <div class="row pt-5 pb-5">
        <div class="col-12 col-sm-4 col-md-3">
            <div class="container">
                <div class="row">
                    <div class="col text-center title">
                        <img class="img-fluid" src="/wp-content/themes/prin-poetes-02/img/logo PdP Tours.png" alt="Grand Livre Poèmes 2017"/>
                        <h2 class="mt-2">Contacts</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-sm-7 col-md-8 col-xl-8 d-flex align-items-center">

            <!-- template must set variable $readmore_link. -->
            <?php include_once(__DIR__.'/front-page-parts/_contacts.php') ?>

        </div>
        <div class="col-12 text-center">
            <?php /*
            <?php if( $readmore_link ) { ?>
                <div >
                    <a href="<?php echo $readmore_link ?>" class="btn read-more" >Voir toutes les ressources</a>
                </div>
            <?php } ?>
            */ ?>
        </div>
    </div>
</section>

<?php

get_footer();
