<?php
/**
 * The page template.
 * 
 * @package pdpat
 */

get_header();

include(__DIR__.'/_nav.php');

use PrinPoetes\Common ;

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <div class="container">
        <div class="row" >
            <div class="col">
                <div class="entry-content">
                    <?php the_title( '<h1>', '</h1>' ); ?>
                </div>    
            </div>            
        </div>
        <div class="row">
            <div class="col">
                <div class="entry-content">
                    <?php the_content(); ?>
                </div>
            </div>            
        </div>
    </div>

</article>

<div class="container">
    <div class="row mt-2 mb-4">
        <div class="col-12">
            <div class="text-center">
                <a href="" class="btn read-more go-back"
                    onclick="window.history.back(); return false;">Retour</a>
            </div>
        </div>
    </div>
</div>

<?php

get_footer();
