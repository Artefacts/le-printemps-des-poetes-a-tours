<?php
use PrinPoetes\Common ;

$datetime = date_create( get_post_meta($post->ID, Common::ACF_DATESTART, true) );

if( $datetime->format('H') == 0 )
    $date_start = $datetime->format('d/m/Y');
else
    $date_start = $datetime->format('d/m/Y à H\hi');

$date_end = get_field(Common::ACF_DATEEND, $post->ID );
if( $date_end )
{
    $datetime = date_create( $date_end );
    if( $datetime->format('H') == 0 )
        $date_end = $datetime->format('d/m/Y');
    else
        $date_end = $datetime->format('d/m/Y à H\hi');
}

?>
<div class="card event mb-3" style="flex-grow: 1;">
    <div class="card-header">
        <?php echo $post->post_title ?>
    </div>
    <div class="card-body text-center">

        <!-- optional thumbnail -->

        <?php
        $thumb_html = get_the_post_thumbnail($post->ID, 'medium');
        if( $thumb_html ) { ?>

            <!-- with thumbnail -->

            <div class="container" >
                <div class="row">
                    <div class="col-12 col-lg-5 p-0">
                        <div class="">
                            <?php echo $thumb_html; ?>
                        </div>
                    </div>
                    <div class="col col-lg-7 p-0">
                        <!-- dates -->
                        <table class="mt-1 ml-auto mr-auto">
                            <tr >
                                <td style="padding: 4px"><span class="dashicons dashicons-calendar-alt"></span></td>
                                <td>
                                <?php if( $date_end ) { ?>
                                    <span style="">du <span class="badge badge-light"><?php echo $date_start ?></span></span>
                                    <br/>
                                    <span style="">au <span class="badge badge-light" ><?php echo $date_end ?></span></span>
                                <?php } else { ?>
                                    <span style="">le <span class="badge badge-light" ><?php echo $date_start ?></span></span>
                                <?php } ?>
                                    
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

        <?php } else { ?>

            <!-- without thumbnail -->
            <!-- dates -->
            <table class="ml-auto mr-auto mt-1">
                <tr >
                    <td style="padding: 4px"><span class="dashicons dashicons-calendar-alt"></span></td>
                    <td>
                    <?php if( $date_end ) { ?>
                        <span style="">du <span class="badge badge-light"><?php echo $date_start ?></span></span>
                        <br/>
                        <span style="">au <span class="badge badge-light" ><?php echo $date_end ?></span></span>
                    <?php } else { ?>
                        <span style="">le <span class="badge badge-light" ><?php echo $date_start ?></span></span>
                    <?php } ?>
                        
                    </td>
                </tr>
            </table>

        <?php } ?>

        <!-- location -->
        <table class="ml-auto mr-auto mt-1">
            <tr>
                <td><span class="dashicons dashicons-location"></span></td>
                <td>
                    <?php echo get_post_meta($post->ID, Common::ACF_LIEU, true) ?>
                </td>
            </tr>
        </table>

        <!-- terms -->
        <ul class="terms mt-2 text-center">
            <?php
            /**
             * @var \WP_Term $term
             */
            foreach( wp_get_post_terms($post->ID, Common::TAX_EVENTS) ?? [] as $term )
            {
            ?>
                <li><?php echo $term->name ?></li> 
            <?php
            }
            ?>
        </ul>
        <div style="text-align: center;">
            <a class="read-more" style="color: #495057" href="<?php echo get_permalink( $post->ID )?>">Voir en détail</a>
        </div>
    </div>
</div>
