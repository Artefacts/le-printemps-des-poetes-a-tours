<?php
/**
 * 
 * @package pdpat
 */

namespace PrinPoetes ;

use PrinPoetes\Common ;

class Front
{
    public static function wp_excerpt_more($more)
    {
        //PrinPoetes::debug(__FUNCTION__, $more );
        global $post;
           //return '… <a href="'. get_permalink($post->ID) . '">' . 'Read More &raquo;' . '</a>';
    
           $link = sprintf(
            '&hellip; <a href="%1$s" class="read-more" target="_top">%2$s</a>',
            esc_url( get_permalink($post->ID) ),
            /* translators: %s: Post title. */
            'Lire la suite <span class="sr-only">de «' . get_the_title() . '»</span>'
        );
        return $link;    
    }

    public static function wp_enqueue_scripts()
    {
        // https://developer.wordpress.org/reference/functions/wp_enqueue_script/

        // loading Gutenberg-related stylesheets.
        // usefull for blocs like ".wp-block-media-text"
        //wp_dequeue_style( 'wp-block-library' );
        //wp_dequeue_style( 'wp-block-library-theme' );

        wp_enqueue_style( 'dashicons' );
        // For embed video 
        wp_enqueue_style( 'wp-embed' );
    }

    /**
     * Ajoute au body une classe en rapport avec:
     * - page : page-<slug>
     * - post : category-<term1> category-<termX>
     */
    public static function wp_body_class( Array $classes )
    {
        /**
         * @var \WP_Post
         */
        global $post ;
        //error_log('Filder body_class: post_type:'.$post->post_type);

        if( empty($post) )
            return $classes;

        // Post categories
        if( $post->post_type == 'post' )
        {
            foreach( wp_get_post_terms($post->ID) ?? [] as $term )
            {
                $classes[] = 'category-'.$term->slug ;
            }
        }
        // Page slug
        else if( $post->post_type == 'page' )
        {
            //error_log( print_r($terms,true));
            
            $classes[] = 'page-'.$post->post_name ;
        }

        return $classes;
    }

    /**
     * Page archives des events.
     * Filtre et ordonne.
     * 
     * @param \WP_Query $query
     * @return void
     */
    public static function pre_get_posts_events( \WP_Query $query )
    {
        //Common::debug(__METHOD__, $query );
        if( ! $query->is_post_type_archive() )
            return ;
        if( $query->get('post_type') != Common::CPT_EVENTS )
            return ;

        $query->set('order' ,'ASC');
        $query->set('meta_key' , Common::ACF_DATESTART);
        $query->set('meta_type', 'DATETIME');
        $query->set('orderby', 'meta_value');

    }
}
