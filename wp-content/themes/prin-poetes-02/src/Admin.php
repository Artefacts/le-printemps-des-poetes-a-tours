<?php
/**
 * 
 * @package pdpat
 */

namespace PrinPoetes ;

use PrinPoetes\Common ;

class Admin
{
    /**
     * Customize admin events list
     *
     * @return void
     */
    public static function events_admin()
    {
        // add columns list
        add_filter('manage_'.Common::CPT_EVENTS.'_posts_columns', function( Array $columns )
        {
            //PrinPoetes::debug('manage',['columns'=>$columns]);
            unset($columns['date']);
            return array_merge($columns, array(
                Common::ACF_LIEU => __('Lieu') ,
                Common::ACF_DATESTART => __('Date début'),
                Common::ACF_DATEEND => __('Date fin'),
                'modified' => __('Mise à jour'),
                'post_date' => __('Publié'),
            ));
        });
        // add fields to admin list columns
        add_action('manage_'.Common::CPT_EVENTS.'_posts_custom_column', function( $column, $post_id )
        {
            switch( $column )
            {
                case Common::ACF_LIEU:
                    echo get_field( Common::ACF_LIEU, $post_id );
                    break;
                case Common::ACF_DATESTART :
                    $datetime = date_create( get_field( Common::ACF_DATESTART, $post_id ) );
                    echo $datetime->format('d/m/Y H:i');
                    break;
                case Common::ACF_DATEEND :
                    $m = get_field( Common::ACF_DATEEND, $post_id );
                    if( $m )
                    {
                        $datetime = date_create( $m );
                        echo $datetime->format('d/m/Y H:i');    
                    }
                    else
                    {
                        echo '';
                    }
                    break;
                case 'modified':
                    //echo get_the_modified_date('d/m/Y H:i', $post_id);
                    echo get_post_datetime($post_id, 'modified')->format('d/m/Y H:i' );
                    break;
                case 'post_date':
                    echo get_post_datetime($post_id, 'date')->format('d/m/Y H:i' );
                    break;
            }
        }, 10, 2);

        add_filter( 'manage_edit-' .Common::CPT_EVENTS. '_sortable_columns', function( Array $columns )
        {
            $columns[Common::ACF_DATESTART] = Common::ACF_DATESTART;
            $columns[Common::ACF_DATEEND] = Common::ACF_DATEEND;
            $columns['modified'] = 'modified';
            $columns['post_date'] = 'date';
            return $columns;
        } );

        add_action( 'pre_get_posts', function( \WP_Query $query )
        {
            if( $query->get('post_type') != Common::CPT_EVENTS )
                return ;

            $orderby = $query->get( 'orderby');
            switch( $orderby )
            {
                case 'post_date':
                    $query->set( 'orderby', 'date' );
                    break;
                case Common::ACF_DATESTART:
                    $query->set( 'meta_key', Common::ACF_DATESTART);
                    $query->set( 'meta_type', 'DATETIME' );
                    $query->set( 'orderby', 'meta_value' );
                    break;
                case Common::ACF_DATEEND:
                    $query->set( 'meta_key', Common::ACF_DATEEND);
                    $query->set( 'meta_type', 'DATETIME' );
                    $query->set( 'orderby', 'meta_value' );
                    break;
    
            }    
        } );
    }

}
