<?php
/**
 * 
 * @package pdpat
 */

namespace PrinPoetes ;

class Common
{
    const CPT_EVENTS = 'events' ;
    const TAX_EVENTS = 'event_types' ;
    const ACF_DATESTART = 'date_start' ;
    const ACF_DATEEND = 'date_end' ;
    const ACF_LIEU = 'lieu' ;

    const SLUG_AGENDA_INTRO = 'agenda-introduction' ;
    const SLUG_QUI_INTRO = 'qui-introduction' ;
    const SLUG_RESSOURCES_INTRO = 'ressources-introduction' ;
    const SLUG_CONTACT_INTRO = 'contacts-introduction' ;
    const SLUG_CONTACT_ARCHIVES = 'archives-introduction' ;
    const SLUG_ECOUTERREGARDER_INTRO = 'ecouter-regarder-introduction' ;

    public static function getFolder()
    {
        static $folder;
        if( empty($folder) )
            $folder = \substr( dirname(__FILE__), strlen(ABSPATH)-1 ) ;
        return $folder;
    }

    public static function wp_after_setup_theme()
    {
        // Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

        add_theme_support( 'title-tag' );

        // Stop Loading wp-emoji-release.min.js and CSS file
        remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
        remove_action( 'wp_print_styles', 'print_emoji_styles' );
    }

    public static function wp_enqueue_scripts()
    {
        // https://developer.wordpress.org/reference/functions/wp_enqueue_script/
        wp_enqueue_script('jquery');
    }

    public static function isDebug()
    {
        static $debug = null ;
        if( $debug == null )
        {
            $debug = (defined('WP_DEBUG')
                ? (WP_DEBUG?true:false)
                : false );
        }
        return $debug ;
    }

    public static function debug( ...$items )
    {
        if( ! self::isDebug() )
            return ;

        $msg = '' ;
        foreach( $items as $item )
        {
            switch ( gettype($item))
            {
                case 'boolean' :
                    $msg.= ($item ? 'true':'false');
                    break;
                case 'NULL' :
                    $msg.= 'null';
                    break;
                case 'integer' :
                case 'double' :
                case 'float' :
                case 'string' :
                    $msg.= $item ;
                    break;
                default:
                    $msg .= var_export($item,true) ;
            }
            $msg.=' ';
        }
        error_log( $msg );
    }

}
