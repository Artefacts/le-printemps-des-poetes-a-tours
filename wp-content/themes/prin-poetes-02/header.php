<?php
/**
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package prin-poetes
 */
?><!doctype html>
<html <?php language_attributes(); ?> >

<head>
  <meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
  <meta name="description" content="Printemps des poètes à Tours" />
	<link rel="profile" href="https://gmpg.org/xfn/11" />
  <link rel="stylesheet" href="/wp-content/themes/prin-poetes-02/vendor/bootstrap-4.6.0/css/bootstrap.min.css" />
  <link rel="stylesheet" href="/wp-content/themes/prin-poetes-02/style.css" />
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?> id="top">
