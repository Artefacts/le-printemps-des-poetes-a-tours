

<?php wp_footer(); ?>

<div id="backtoTop">
    <a href="#top" class="page-scroll">
        <span class="dashicons dashicons-arrow-up-alt2"></span>
    </a>
</div>

<script src="/wp-content/themes/prin-poetes-02/vendor/bootstrap-4.6.0/js/bootstrap.min.js" ></script>

<script>
jQuery(function($)
{
    var offset = 60 ;
    var backtoTop = document.getElementById('backtoTop');

    $('a.page-scroll').bind('click', function(event)
    {
        event.preventDefault();
        var $ele = $(this);
        $('html, body').stop().animate({
            scrollTop: ($($ele.attr('href')).offset().top - offset )
        }, 700 );
    });

    window.onscroll = function()
    {
        if (document.body.scrollTop > offset || document.documentElement.scrollTop > offset )
            backtoTop.style.display = 'block';
        else
            backtoTop.style.display = 'none';
    };

});
</script>
</body>
</html>
