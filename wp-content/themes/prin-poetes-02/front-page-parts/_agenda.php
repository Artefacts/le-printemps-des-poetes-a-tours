<?php
/**
 * template must set variable $readmore_link.
 */

use PrinPoetes\Common ;

$readmore_link = get_post_type_archive_link( Common::CPT_EVENTS ) ;

//Common::debug('_agenda', $readmore_link );

$posts_per_page = 6 ;
$now = date('Y-m-d H:i:s');

$q = new \WP_Query([
    'post_type' => Common::CPT_EVENTS,
    'posts_per_page' => $posts_per_page,
    'order' => 'ASC',
    'meta_key' => Common::ACF_DATESTART,
    'meta_type' => 'DATETIME',
    'orderby' => 'meta_value',
    'meta_query' => [
        'relation' => 'OR',
        [
            'key' => Common::ACF_DATESTART,
            'compare' => '>=',
            'value' => $now,
            'type' =>'DATETIME'        
        ],
        [
            'key' => Common::ACF_DATEEND,
            'compare' => '>=',
            'value' => $now,
            'type' =>'DATETIME'        
        ],
],
]);
$events = $q->get_posts();
//$events = array_reverse($events );

if( ! empty($events) )
{
    $oldLocale = setlocale( LC_ALL, 0 );
    setlocale( LC_ALL, 'fr_FR.utf8' );
    /**
     * @var \WP_Post $p
     */
?>
<div class="row">
<?php
    foreach( $events as $post )
    {
?>
<div class="col-12 col-lg-6" style="display: flex; flex-flow: column;">

        <?php include(__DIR__.'/../_event.php') ?>

</div>
<?php
            
    }
    setlocale( LC_ALL, $oldLocale );
?>
</div><!--row-->
<?php    
}
wp_reset_postdata();
