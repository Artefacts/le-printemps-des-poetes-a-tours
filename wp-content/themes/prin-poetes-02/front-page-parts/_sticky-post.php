<?php
/**
 * Pour le "coup de coeur" on utilise un sticky post.
 */
$sticky = get_option( 'sticky_posts' );

if( !empty($sticky) && isset($sticky[0]) )
{

    /**
     * @var \WP_Post $post ;
     */
    $post = get_post( $sticky[0] );
    echo $post->post_content ;
}
else
{ // No sticky post

}
?>
