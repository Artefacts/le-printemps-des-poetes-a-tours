<?php
/**
 * template must set variable $readmore_link.
 */
use PrinPoetes\Common ;

$post = \get_page_by_path(Common::SLUG_RESSOURCES_INTRO);
$post_child = get_pages(['child_of'=>$post->ID,'depth'=>1]);

//Common::debug('front-page', count($post_child) );
if( empty($post_child) || (! is_array($post_child)) )
{
    $post_child = null ;
    $readmore_link = null ;
}
else
{
    $post_child = $post_child[0];
    $readmore_link = \get_permalink( $post_child );
}
?>
<div class="card mb-3" >
    <div class="card-body">
        <?php
            $content = get_the_content( null, null, $post );
        	$content = apply_filters( 'the_content', $content );
            echo $content ;
        ?>
    </div>
</div>